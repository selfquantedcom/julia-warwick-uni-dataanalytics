using Plots
using LaTeXStrings
using Distributions
using DataFrames
using DecisionTree

#Q1
d = Normal(0.0, 1.0)
x = rand(d, 10)

#Q2
# YOUR CODE HERE
#scale by: x = ln(1-u)/(−λ)
function rexp(N)
    result = zeros(N)
    for i in 1:N
        u = rand(0:99)/100
        x = log(1-abs(u))/(-1.234)
        result[i] = x
    end
    return result
end

#Q3
M = 1_000
N = 1_000
y = zeros(M)

# YOUR CODE HERE
for i in 1:M
    y[i] = exp(1.234)/N#rexp(1)[1]/N
end


fit(Normal, y)


#Q4

# YOUR CODE HERE

function diceroll(n)
    sum = 0
    for i in 1:n
        sum += rand(1:6)
    end
    return sum
end

function mc_dice(n,m)
    trials = 10000
    count = 0
    for i in 1:trials
        x = diceroll(n)
        if(x==m)
            count += 1
        end
    end
    probability = count/trials
    return probability
end

#configure accuracy
function mc_dice_adjuster(n,m,t)
    counter = 0
    #run 100 trials aiming for at least 95% accuracy i.e. confidence interval = 5%
    for var in 1:100
        trials = t
        count = 0
        for i in 1:trials
            x = diceroll(n)
            if(x==m)
                count += 1
            end
        end
        #determine if error is less than 0.01
        probability = count/trials
        if(abs(probability - 1/6)<0.01)
            counter += 1
        end
    end
    #is accuracy at least 95%?
    if counter>=95
        return true
    else
        return false
    end
end

#test
@show mc_dice_adjuster(1,3,1)
@show mc_dice_adjuster(1,3,10)
@show mc_dice_adjuster(1,3,100)
@show mc_dice_adjuster(1,3,1000)
@show mc_dice_adjuster(1,3,10000)
@show mc_dice_adjuster(1,3,100000)

#conclude:
#successive increasing of trials by a factor of 10 leads to
#a statistically significant result at 5% confidence interval when
#number of trials >= 10,000

#Q5
# YOUR CODE HERE
function survival_by_class(titanic, num)
    class_count = 0
    sum = 0
    N = nrow(titanic)
    for i in 1:N
        if(titanic[i,3]==num)
            class_count += 1
            if(Int(titanic[i,2]) == 1)
                sum += 1
            end
        end
    end
    proportion = sum/class_count
    return proportion
end

#Q5
# YOUR CODE HERE
#try titanic[:Familysize] = 0, to append the  column

for i in 1:nrow(titanic)
    titanic[i, end] = titanic[i,7] + titanic[i,8] + 1
end

#Q6b .....

#Q7a
# YOUR CODE HERE
titanic[:Title] = ""

for i in 1:nrow(titanic)
    word = split(titanic[i,4],".")
    word = split(word[1])
    word = word[end]


    if(word=="Mlle")
        word = "Miss"
    elseif(word == "Mme")
        word = "Mrs"
    elseif(word == "Ms")
        word = "Miss"
    end
    titanic[i,end] = word
end



# YOUR CODE HERE 7b
function survival_by_title(titanic, title)
    title_count = 0
    sum = 0
    N = nrow(titanic)
    for i in 1:N
        if(titanic[i,end]==title)
            title_count += 1
            if(Int(titanic[i,2]) == 1)
                sum += 1
            end
        end
    end
    proportion = sum/title_count
    return proportion
end
@show survival_by_title(titanic,"Mr")
@show survival_by_title(titanic,"Miss")
@show survival_by_title(titanic,"Mrs")

#Q8
# YOUR CODE HERE
head(titanic)
fit(Normal, titanic[6])

mean_age = fit(Normal, titanic[6])


#6b
# YOUR CODE HERE

#get max family size to determine histogram "bins"
maximum = 0
for i in 1:nrow(titanic)
    if(Int(titanic[i,end])>maximum)
        maximum = titanic[i,end]
    end
end

#How many survived?
#survived = 0
#for i in 1:nrow(titanic)
#    if(Int(titanic[i,2])==1)
#        survived += 1
#    end
#end


#determine relative survival for each group based on family size
#famSize_survival = zeros(maximum)
#for j in 1:maximum
#    for i in 1:nrow(titanic)
#        if(Int(titanic[i,length(titanic)-1]) == j)
#            if(Int(titanic[i,2])==1)
#                famSize_survival[j] += 1
#            end
#        end
#    end
#end

#calculate relative survival frequencies for each family size
#for i in 1:length(famSize_survival)
#    famSize_survival[i] = famSize_survival[i]/survived
#end


#plot bar chart aka histogram
#x = [1, 2, 3, 4, 5, 6, 7, 8]
#y = famSize_survival
#bar(x,y)

histogram(titanic[:FamilySize], xlabel="Family size", ylabel="Frequency", bins=maximum, legend=false)
