using Plots
using DifferentialEquations
using LsqFit

population = [1.65, 2.52, 5.98, 6.70, 6.90, 7.05, 7.48]
year = [1900, 1950, 1999, 2008, 2010, 2012, 2017];

P0 = population[1] # world population in 1900
model(x, p) = P0*exp.(p[1].*x)
fit = curve_fit(model, year .- 1900, population, [0.01])
α = fit.param[1] # estimated population growth rate
#Q1
#plot(year,population,seriestype=:scatter, xlabel="Year", ylabel = "Population", title="Population Growth")
#curve_fit() documentation: https://github.com/JuliaNLSolvers/LsqFit.jl/commit/1f6b1e0267bb790117654a4dce7c8c5fff4222db
#YOUR CODE HERE
P0 = population[1] # world population in 1900
t=0:1000:20000
dP1(t, P) = α*P
prob = ODEProblem(dP1, P0, (1900.,2100.))
sol1 = solve(prob);

plot!(sol1, xlabel="Year", ylabel = "Population Bn", label="exponential growth model", title="Population Growth")

plot(sol1, xlabel="Year", ylabel = "Population Bn", label="exponential growth model", title="Population Growth")
plot!(sol2, xlabel="Year", ylabel = "Population Bn", label="capped growth model", title="Population Growth")


plot(sol1, xlabel="Year", ylabel = "Population Bn", label="exponential growth model", title="Population Growth")
plot!(sol2, xlabel="Year", ylabel = "Population Bn", label="capped growth model", title="Population Growth")
plot!(sol3, xlabel="Year", ylabel = "Population Bn", label="declining growth model", title="Population Growth")






#YOUR CODE HERE
plot(year,population,seriestype=:scatter, xlabel="Year", ylabel = "Population", label="historic data", title="Population Growth")
plot!(year, sol1.t,seriestype=:scatter, xlabel="Year", ylabel = "Population", label="estimated data", title="Population Growth")

K = 11 # population cap in billions of people (carrying capacity)
#YOUR CODE HERE
P0 = population[end] # world population in 2017
dP2(t, P) = α*P*(1-P/K)
prob = ODEProblem(dP2, P0, (2017.,2100.))
sol2 = solve(prob);

α0 = α    # initial growth rate
dα = 0.001 # change of growth rate per year
#YOUR CODE HERE
alpha_lin(t) = α - (t-2017)*dα

#YOUR CODE HERE
P0 = population[end] # world population in 2017
dP3(t, P) = alpha_lin(t)*P*(1-P/K)
prob = ODEProblem(dP3, P0, (2017.,2100.))
sol3 = solve(prob);

#YOUR CODE HERE

function pop5bn(sol3)
    counter = 0
    for item in sol3.u
        counter += 1
        if item<5.
            return Int.(floor(sol3.t[counter]))
        end
    end
end


@show sol3.u
@show sol3.t
@show pop5bn(sol3)
