# Set up system properties - we're using atomic units, where hbar = 1.0.
#
mass = 1.0
hbar = 1.0

# Setup potential parameter.
eta = 1.3544

# Set up the uniform grid:
Xmax = 5.0
Xmin = -5.0
Length = Xmax - Xmin
ngrid = 101
x = linspace(Xmin, Xmax, ngrid)
dx = x[2] - x[1]

# YOUR CODE HERE
#Hij = Tij + Vxi * Phi(x)
#when i=j: Tij = pi^2/6*delta(x^2). Else: (-1)^(i-j)/delta(x^2)*(i-j)^2
#Vxi = (1/16*1.3544)*x^4 - 0.5*x^2
#Phi(x) = e^(i*k(x-j*delta(x)))/2*K^0.5 in range -K to +K Where K = pi/delta(x), k = 1



kspring = 1.0
kmax = 100
xloc = 0.0 #0.1 from the lecture
y = zeros(Complex, ngrid)

for i in -kmax:kmax
    y .+= exp.(im * i * (x-xloc)) # dx or x-xloc?
end

#phi(x):
#z = real(z)

#plot
font = Plots.font("Helvetica", 16)
pyplot(guidefont=font, xtickfont=font, ytickfont=font, legendfont=font)
plot(x, real(y), xlim=[Xmin,Xmax],
     xlabel=L"x", ylabel=L"$\phi(x)$",
     lw=2, legend=false)
#
y = zeros(4, ngrid)
y[1, :] = exp.(-0.5x.^2)
y[2, :] = exp.(-0.5x.^2) .* x
y[3, :] = exp.(-0.5x.^2) .* (2*x.^2 - 1)
y[4, :] = exp.(-0.5x.^2) .* (2*x.^3 - 3*x)

# COLBERT-MILLER DVR HAMILTONIAN CONSTRUCTION.
#
# Set up potential energy matrix.

V = zeros(ngrid, ngrid)
for i = 1:ngrid
    V[i, i] = (1/(16*eta))*x[i]^4-0.5*x[i]^2
end
diagind

# Set up kinetic energy matrix.
T = zeros(ngrid, ngrid)
for i = 1:ngrid
    for j = 1:ngrid
        if i == j
            T[i, j] = ((hbar^2) * (-1.0)^(i-j) * π^2) / (6 * mass * dx^2 )
        else
            T[i, j] = ((hbar^2) * (-1.0)^(i-j)) / (mass * dx^2 * (i-j)^2 )
        end
    end
end



# Normalize each function.
#for n=1:4
#    y[n, :] ./= sqrt(sum(y[n, :].^2 * dx))
#end


# Create the Hamiltonian matrix:
H = T + V;

# Solve the eigenvalue problem
E, c = eig(H)

# YOUR CODE HERE
# Normalize each eigenfunction using simple quadrature.
for i = 1:ngrid
    c[:, i] ./= sqrt(sum(c[:, i].^2 * dx))
end

p = plot(x, 0.5*x.^2, label=L"V(x)",
         xlabel="x", ylabel="P(x)", lw=2,
         linestyle=:dash, legend=:topright, xlims=(-5, 5), ylims=(0, 4))
for n=1:4
    plot!(x, c[:, n].^2 + (n-1), lw=2, label="n=$n")
end
display(p)


# YOUR CODE HERE
#ASK IN SEMINAR: IS THERE A MISTAKE IN THE TEST???? MY VALUES MATCH EXCEPT FOR THE SIGN OF THE FIRST ELEMENT

# Initialize wavefunction
alpha = 0.5
mu = (-2.5)
distribution = (((2.0*alpha)/pi)^(1/4)) * exp.((-1)*alpha * ((x - mu).^2))

# Calculate the expansion coefficients of each eigenvector using simple integration.
coeff = zeros(Complex, ngrid)
for i = 1:ngrid
    coeff[i] = sum(distribution .* c[:, i]) * dx
end

# YOUR CODE HERE
#plot(x, distribution, fillrange=0, fillcolor=:green, alpha=0.5, color=:black, lw=3)
#title!("Initial wavefunction before propagation")

gr()
p = plot(x, [(1/16*1.3544)*x.^4 - 0.5*x.^2 ], lw=3, legend=true,
               xlabel="x", ylabel="P(x)", legend=:topleft,label="V(x)",
             alpha=5, ylim=(-1, 2), color="black")
increase_opacity = 0.005
for tt in 0.0:20.0:100.0
    p = plot!(x, [abs.(wavefunction(tt))], lw=3, legend=true, legend=:topright,
                 xlabel="x", ylabel="P(x)", alpha=0.3+tt*increase_opacity, ylim=(-1, 2), xlim=(-5,7), label="t=" * string(tt), color="red")

end
display(p)

#step t=20 was chosen to show progression over time but not to visually overwhelm as smaller increments would do.
#rather than using different color for each step, increasing opacity was used as we are still mapping the same variable.
#Using the legend it is clear that waves with higher opacity are the more recent ones i.e. those with higher time elapsed.
#xaxis range (-5,7) enables proper dispaly of the legend without overlaping with the chart while
#at the same time not "zooming out" too much. Similar applies to yaxis range. 
