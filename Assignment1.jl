#Q1
volball(r) = (4*pi*r^3)/3
print(volball(0.4))

#Q2
function quadratic(a, b, c)
    return (-b-sqrt(b^2-4*a*c))/(2*a), (-b+sqrt(b^2-4*a*c))/(2*a)
end

print(quadratic(1,2,0.5))

#Q3
#Answer is: You have a long name.

#Q4
s = "abcdefg"
t = "cde"
#s = "cat"
#t = "mouse"
#s = "new york"
#t = "new"

function strangefunction(s, t)
    i = searchindex(s, t)
    if i <= 1
        print(nothing)
    else
        print(s[i-1])
    end
end
strangefunction(s,t)


'''Searchindex is a function which returns the beginning position (i.e. a nuber/index) of string T within string S.

When T is not subset of S or when T starts at S[1], STRANGEFUNCTION will return NOTHING.

Otherwise, STRANGEFUNCTION will return a letter of string S, prior to the beginning of its sub-string T (i.e. letter at S[i-1] where the beginning position of T is at S[i].

Output: b, nothing, nothing.'''

#Q5
for i in 1:10
    println(i^2)
end

#Q6
x = zeros(10)
for i in 1:10
    x[i] = i^2
end

#Q7
left_matrix =   [1 1 -5; 2 -3 0; 0 1 5]
right_vector = [1,2,-1]
x = left_matrix\right_vector
print(x)

#Q8
# YOUR CODE HERE
function solve_system(N)
    #create matrix Aij = 1/(i+j)
    A = zeros(N,N)
    for i in 1:N
        for j in 1:N
            A[i,j] = 1/(i+j)
        end
    end
    #create vector of length N with increasing values
    b = zeros(N,1)
    for i in 1:N
        b[i] = i
    end
    #create identity matrix with N dim
    I = eye(N,N)

    result = (I + A)\b
    return result
end
