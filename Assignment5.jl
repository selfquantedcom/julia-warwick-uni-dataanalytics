#Q1 YOUR CODE HERE
using Plots
gr();

function MyMeanAndVar(m_1, m_2, m_3, s_1, s_2, s_3)
    m_4=m_1+m_2+m_3 # mean for the sum
    s_4=sqrt(s_1^2+s_2^2+s_3^2) # standard deviation for the sum
    return m_4, s_4
end

# use this cell to test your answer
#TESTING
# define an inline function that generates general Gaussian distributions
g(xx,m,s)=exp.(-(xx-m).^2/(2s^2))/sqrt(2*pi*s^2)
xx=collect(-3:0.01:13)
#yy=g(xx,m,s)

# parameters for the data
m_1,s_1=-4.0,1.0 # first type of random number
m_2,s_2=4.0,1.0  # second type of random number
m_3,s_3=0.0,1.0

# generate the samples
nsamples=1000
x1=m_1+s_1*randn(nsamples)
x2=m_2+s_2*randn(nsamples)
x3=m_3+s_3*randn(nsamples)

# the theory
m_4, s_4 = MyMeanAndVar(m_1, m_2, m_3, s_1, s_2, s_3)

xx=collect(-20:0.01:20)
yy1=g(xx,m_1,s_1)
yy2=g(xx,m_2,s_2)
yy3=g(xx,m_3,s_3)
yy4=g(xx,m_4,s_4)

# some plots
histogram(x1+x2+x3,normed=true,bins=20,label="sum of samples")
plot!(xx,yy1,label="theory for # 1")
plot!(xx,yy2,label="theory for # 2")
plot!(xx,yy3,label="theory for # 3")
plot!(xx,yy4,label="theory for their sum",color=[:black])



#Q2 YOUR CODE HERE
# YOUR CODE HERE
using Distributions
N = 4
function MyPi(N)
    ns = 10^2 #should be 10^5
    d = Binomial(1,0.5)
    x = rand(d,1)
    @show x

    #THIS
    #y = sqrt(2/N*pi)*exp(-2*(x^2)/N)

    #OR THIS
    counter = 0
    for i in 1:ns
        if(x[i]==0)
            counter += 1
        end
    end

    #@show 2/(N*(counter/ns)^2)

    #OR GENERATE N NORMALLY DISTRIBUTED RANDOM NUMBERS. THIS REPRESENTS ONE SAMPLE
    #MAKE ns OF THESE SAMPLES. THE PROPORTION OF THESE SAMPLES, FOR WHICH SUM=0, REPRESENTS PI
    sample_sum = zeros(N)
    proportion_0 = 0
    for i in 1:ns
        for j in 1:N
            sample_sum[j] = x[1]
            if(j==N)
                sample_sum = sum(sample_sum)
                if(sample_sum==0)
                    proportion_0 += 1
                else
                    sample_sum = 0
                end
            end
        end
    end

    @show proportion_0/ns



    return #piN  # (your approx. to pi)
end
MyPi(N)


#ALTERNATIVE Q2
# YOUR CODE HERE
using Distributions

N = 6
function MyPi(N)
    ns = 10^5 #should be 10^5
    d = Binomial(1,0.5)
    x = rand(d,1)


    #OR GENERATE N NORMALLY DISTRIBUTED RANDOM NUMBERS. THIS REPRESENTS ONE SAMPLE
    #MAKE ns OF THESE SAMPLES. THE PROPORTION OF THESE SAMPLES, FOR WHICH SUM=0, REPRESENTS PI


    sample_sum = zeros(N)
    proportion_0 = 0
    for i in 1:ns
        for j in 1:N
            x = rand(d,1)
            sample_sum[j] = x[1]
            #@show sample_sum
            if(j==N)
                sample_sum = sum(sample_sum)
                #@show sample_sum
                if(sample_sum==0)
                    proportion_0 += 1
                    sample_sum = zeros(N)
                else
                    sample_sum = zeros(N)
                end
            end
        end
    end

    @show proportion_0
    @show ns
    @show 2/(N*100*((proportion_0/ns)^2))



    return #piN  # (your approx. to pi)
end
MyPi(N)

#Q2 THIS WORKS
# YOUR CODE HERE
using Distributions

N = 100
function MyPi(N)
    ns = 10^5 #should be 10^5
    d = Binomial(1,0.5)
    x = rand(d,1)


    #OR GENERATE N NORMALLY DISTRIBUTED RANDOM NUMBERS. THIS REPRESENTS ONE SAMPLE
    #MAKE ns OF THESE SAMPLES. THE PROPORTION OF THESE SAMPLES, FOR WHICH SUM=0, REPRESENTS PI

    sample_sum = zeros(N)
    proportion_0 = 0
    for i in 1:ns
        for j in 1:N
            x = rand(d,1)
            if(x[1] == 1)
                sample_sum[j] = 0.5
                else sample_sum[j] = -0.5
            end
            #@show sample_sum
            if(j==N)
               #@show sample_sum
                sample_sum = sum(sample_sum)
                #@show sample_sum
                if(sample_sum==0)
                    proportion_0 += 1
                    sample_sum = zeros(N)
                else
                    sample_sum = zeros(N)
                end
            end
        end
    end
    Fn = proportion_0/ns

    piN = 2/(N*(Fn^2))
    return piN  # (your approx. to pi)
end
MyPi(N)

#Q3
# YOUR CODE
#VAR: 2D=pq. MEAN: p-q=v. Times t for end period, or dt for interim
#there are t/dt hops in a time period of t with a time step dt

# parameters
D = 5
v = 3
dt = 0.01


function MyPandQ(v,D,dt)
    p = (abs(-v+sqrt(v^2+8*D))/2)/10 #/10 to convert to % as sqrt(100)=10
    q = (abs(-v-sqrt(v^2+8*D))/2)/10
   return p, q
end

#@show MyPandQ(v,D,dt)

#TEST VIA SIMULATION
#HYPOTHESIS: mean = p-q
#p = 0.4 i.e. 0.4 chance of getting p
#q = 0.2 i.e. 0.2 chance of getting q

pBin = Binomial(1,0.4) #represents p
qBin = Binomial(1,0.2) #represents q

range = 100000 #number of simulations
particle_array = zeros(range)

for i in 1:range
    #in 0.4 cases both p and q will be zero. Hence the particle will not move and its position change will be 0
    p_temp = rand(pBin,1)[1]
    q_temp = rand(qBin,1)[1]
    particle_array[i] = p_temp-q_temp #final position of the particle at dt represetned by p-q
end

@show sum(particle_array)/range #average position at dt
#p = 0.4, q = 0.2. The mean tends to 0.2 which is p - q





#visualize
#DOES IT ACCOUNT FOR THE POSSIBILITY OF NO MOVE??????

dt,T=0.01,50.0 # various time quantities
nt=Int(T/dt)
t=dt*collect(1:nt)
L=200 # lattice size

p = MyPandQ(v,D,dt)[1]
q = MyPandQ(v,D,dt)[2]
S=zeros(nt,L)   # this is the lattice space in time: column=time, row=lattice entry
S[1,Int(L/2)]=1 # initialise with particle in the middle

for k=1:nt-1 # loop over time
    for j=1:L-1 # loop of L-1 pairs

        s1=rand(1:L-1) # get a random adjacent pair of sites
        s2=s1+1

        S1,S2=S[k,s1],S[k,s2] # get their states

        # see if the particle hops left or right
        if (S1==0)&(S2==1)&(rand()<p)
            S[k,s1],S[k,s2]=S2,S1
        elseif (S1==1)&(S2==0)&(rand()<p)
            S[k,s1],S[k,s2]=S2,S1
        end

    end
    S[k+1,:]=S[k,:]
end

# plot out a time-space map of the diffusion
#heatmap(S,xlabel="position",ylabel="time steps")
