
using Plots
#Q1
# YOUR CODE HERE
function omega_p(n)
    ϵ0=8.9*(1/10)^(8.0)
    q0=(-1.6)*(1/10)^(19.0)
    m0=9.1*(1/10)^(31.0)
    ωp = ((n*q0^2)/(m0*ϵ0))^(0.5)
    return ωp
end

# YOUR CODE HERE
@show solar_corona = omega_p(10^15)
@show gas_jets = omega_p(10^25)
@show fusion_reactor = omega_p(10^32)


#Q2
# YOUR CODE HERE
function dispersion(k, omega_pe, v_wave, wave_type)
    ωpl = ((omega_pe^2)+(v_wave^2)*(k^2))^0.5
    ωEM = ((omega_pe^2) + (3*(10^8))^2*(k^2))^0.5
    ωs = abs(k)*v_wave
    result = 0
    if(wave_type == 'p')
        result = ωpl
        end
    if(wave_type == 'e')
        result =  ωEM
    end
    if(wave_type == 's')
        result = ωs
    end
    return result
end

# YOUR CODE HERE
plotlyjs()
omega_pe = 2 *pi * 10^8
v_wave_em = 3e8
v_wave_pl = 1e7
v_wave_s = 1e6
#k = linspace(-1.0,1.0,101)
k = linspace(-2.0,2.0,100)

#w = dispersion(k, omega_pe, v_wave_em, 'e')

@show ωe = ((omega_pe^2) + (3*(10^8))^2*(k^2))^0.5
#plot(k, ωe, c=:blue, label="Ey", xlab="k (normalised units)", ylab ="omega (normalised units)")

#k = linspace(-2.0,2.0,100)
#w = dispersion(k, omega_pe, v_wave_pl, 'p'))
#ωp = ((omega_pe^2)+(v_wave_pl^2)*(k^2))^0.5
#plot!(k, ωp, c=:red, label="Ey", xlab="k (normalised units)", ylab ="omega (normalised units)")

#w = dispersion(k, omega_pe, v_wave_s, 's')
#ωs = abs(k)*v_wave_s
#plot!(k, ωs, c=:green, label="Ey", xlab="k (normalised units)", ylab ="omega (normalised units)")

# YOUR CODE HERE
omega_pe = 2 *pi * 10^8
v_wave_em = 3e8
k1 = 1
k2 = 1.001
dispersion1 = dispersion(k1, omega_pe, v_wave_em, 'e')
dispersion2 = dispersion(k2, omega_pe, v_wave_em, 'e')
#ωEM = ((omega_pe^2) + (3*(10^8))^2*(k^2))^0.5
v_g = (dispersion1 - dispersion2)/(k1-k2)

# YOUR CODE HERE
k = linspace(-1.0,1.0,101)
plasma = dispersion(1, (2^0.5), 0.105, 'p')
sound = dispersion(1, (2^0.5), 0.105, 's')

plot(k, plasma, c=:orange, label="Ey", xlab="k (normalised units)", ylab ="omega (normalised units)", xlim=[-0.5,0.5])
plot(k, sound, c=:purple, label="Ey", xlab="k (normalised units)", ylab ="omega (normalised units)", xlim=[-0.5,0.5])

# YOUR CODE HERE
v0 = 10^7
x = 0
m0 = 9.10938356 * ((1/10)^31)
q0 = 1.60217662 *((1/10)^19)
tspan = [0,20]
h = 1
E = 2*((1/10)^6)
function dv_dt(t, x, v)
    a = (-q0*E)/m0
    return a
end


function dx_dt(t, x, v)
    v = v0 + t*dv_dt(t, x, v)
    return v
end

@show dv_dt(1, 1, 1)
@show dx_dt(0, 0, 0)

# YOUR CODE HERE

# YOUR CODE HERE

omega_pe = 2 *pi * 10^8
v_wave_em = 3e8
v_wave_pl = 1e7
v_wave_s = 1e6
k = linspace(-1.0,1.0,101)
#k = linspace(-2.0,2.0,100)


plotlyjs()
ωpl = sqrt((omega_pe^2)+(v_wave_em^2)*(k.^2))
plt1 = plot(k, ωpl, c=:blue, label="EM", xlab="k (normalised units)", ylab ="omega (normalised units)")
display(plt1)
ωs = abs(k)*v_wave_s
plt2 = plot(k, ωs, c=:green, label="SW", xlab="k (normalised units)", ylab ="omega (normalised units)", reuse = false)
display(plt2)

# YOUR CODE HERE
k = linspace(-1.0,1.0,101)


ωpl = sqrt(1+k.^2)
ωs = 0.105*abs(k)

plt1 = plot(k, ωpl, c=:orange, label="PL", xlab="k (normalised units)", ylab ="omega (normalised units)", xlim=[-0.5,0.5])
#display(plt1)
plt2 = plot!(k, ωs, c=:purple, label="SW", xlab="k (normalised units)", ylab ="omega (normalised units)", xlim=[-0.5,0.5])
#display(plt2)

# YOUR CODE HERE
omega_pe = 2 *pi * 10^8
v_wave_em = 3e8
v_wave_pl = 1e7
v_wave_s = 1e6
ωpl = sqrt((omega_pe^2)+(v_wave_pl^2)*(0.35^2))
plt3 = plot!([0,0.35],[0,ωpl], c=:red, label="Line", xlab="k (normalised units)", ylab ="omega (normalised units)", xlim=[-0.5,0.5])
#display(plt3)

# YOUR CODE HERE
v0 = 10^7
x = 0
x0 = 0
q0 = 1.6e-19       #Fundamental charge
m0 = 9.1e-31       #Mass of an electron
tspan = [0,20]
h = 0.01
E = 2*((1/10)^6)
y0 = 0

function dv_dt(t, x, v)
    a = (-q0*E)/m0
    return a
end

@show a = (-q0*E)/m0

function dx_dt(t, x, v)
    v1 = v0 + t*((-q0*E)/m0)
    return v1
end

# YOUR CODE HERE
m0 = 9.1e-31
q0 = 1.6e-19


function B(x)
    B0 = (1/10)^8
    a = 10^6
    result = B0*(1+a*x)^2
end


function gyroperiod(x, B)
    T_gyro = (2 * pi * m0)/(q0*B(x))
    return T_gyro
end
