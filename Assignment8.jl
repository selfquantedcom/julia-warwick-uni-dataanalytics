# YOUR CODE HERE
using GLM
using DataFrames #NB: we are using pinned version 0.9.1 to match JuliaBox
using FreqTables
using Plots
using StatPlots
using Distributions
using RDatasets
using Suppressor
using DecisionTree

#MISSING SUMMARY STATISTICS!!!!!!!!!!!!!!!!!!!
#create a duplicate dataframe without missing values
#mpg = 1 horsepower = 4
auto=readtable("Auto.csv")

hp = zeros(nrow(auto)-5)
mp = zeros(nrow(auto)-5)

j = 1
for i in 1:nrow(auto)
    if (String(auto[i,4]) == "?")
        j = j - 1
    else
        hp[j] = float(auto[i,4])
        mp[j] = float(auto[i,1])
    end
    j = j + 1
end

df = DataFrame(horsepower=hp, mpg=mp)
@suppress stats =lm(@formula(mpg~horsepower),df)

@show stats

#visualise results
p1=scatter(df[:horsepower],df[:mpg],)
xs=[1.0:250;]
plot!(xs,GLM.predict(stats,DataFrame(horsepower=xs)))

#ANSWERS
#A: yes
#B:
#C: negative
#D

#predicted = 0.0
#lower = 0.0
#upper = 0.0

#Q2
using DataFrames
srand(1)
boston = readtable("boston.csv")
features = convert(Array,boston[:,1:13])
labels = convert(Array,boston[:,13])

function nfoldCV(classifier::Symbol,labels, features,maxdepth, args...)
    nfolds = args[end]
    if nfolds < 2
        return nothing
    end
    if classifier == :tree
        pruning_purity = args[1]
    elseif classifier == :forest
        nsubfeatures = args[1]
        ntrees = args[2]
        partialsampling = args[3]
    elseif classifier == :stumps
        niterations = args[1]
    end
    N = length(labels)
    ntest = _int(floor(N / nfolds))
    inds = randperm(N)
    accuracy = zeros(nfolds)
    for i in 1:nfolds
        test_inds = falses(N)
        test_inds[(i - 1) * ntest + 1 : i * ntest] = true
        train_inds = (!).(test_inds)
        test_features = features[inds[test_inds],:]
        test_labels = labels[inds[test_inds]]
        train_features = features[inds[train_inds],:]
        train_labels = labels[inds[train_inds]]
        if classifier == :tree
            model = build_tree(train_labels, train_features, 0,maxdepth)
            if pruning_purity < 1.0
                model = prune_tree(model, pruning_purity)
            end
            predictions = apply_tree(model, test_features)
        elseif classifier == :forest
            model = build_forest(train_labels, train_features, nsubfeatures, ntrees, partialsampling)
            predictions = apply_forest(model, test_features)
        elseif classifier == :stumps
            model, coeffs = build_adaboost_stumps(train_labels, train_features, niterations)
            predictions = apply_adaboost_stumps(model, coeffs, test_features)
        end
        cm = confusion_matrix(test_labels, predictions)
        accuracy[i] = cm.accuracy

    end

    return accuracy
end


# YOUR CODE HERE
train = rand(nrow(boston)) .< 0.5 # make this random 50%
test = .!train
traindf= boston[train,:];
testdf=boston[test,:] ;

cols=[1:13;]
trainErr= Float64[];testErr= Float64[]; cvErr= Float64[];
labelsTest = convert(Array, testdf[:, end]);
featuresTest = convert(Array, testdf[:, cols]);
treeSize=50

for i=1:treeSize
    model=build_tree(labels, features,0,i)

    if(i<7 )
        print("============== Depth=$i ===========\n");print_tree(model);
    end

    push!(trainErr,1-confusion_matrix(labels,DecisionTree.apply_tree(model,features)).accuracy)
    push!(testErr,1-confusion_matrix(labelsTest,DecisionTree.apply_tree(model,featuresTest)).accuracy)
    push!(cvErr,1-mean(nfoldCV(:tree,labels, features,i,5)))
end



#bagged =     # make sure to save the result of question 1 in `bagged`
#rf =         # make sure to save the result of question 2 in `rf`

#Q2 RANDOM FOREST
using DataFrames
srand(1)
boston = readtable("boston.csv")
features = convert(Array,boston[:,1:13])
labels = convert(Array,boston[:,14])

# YOUR CODE HERE

#USING RANDOM FOREST
train = rand(nrow(boston)) .< 4/5 #make this random 20%
test = .!train
testdf=boston[test,:]
cols=[1:13;]

featuresTest = convert(Array, testdf[:, cols])
labelsTest = convert(Array, testdf[:, end])

rf_model = build_forest(labels, features, 2, 50, 1.0, 6)
# using 2 random features, 50 trees, 1.0 portion of samples per tree (optional), and a maximum tree depth of 6 (optional)
# apply learned model
confusion_matrix(labelsTest,apply_forest(rf_model,featuresTest)).accuracy


#@show head(boston)

using DataFrames
srand(1)
boston = readtable("boston.csv")
features = convert(Array,boston[:,1:13])
labels = convert(Array,boston[:,14])

# YOUR CODE HERE
train = rand(nrow(boston)) .< 4/5 #make this random 20%
test = .!train
testdf=boston[test,:]
cols=[1:13;]

featuresTest = convert(Array, testdf[:, cols])
labelsTest = convert(Array, testdf[:, end])


#USING BAGED TREES
B=50
maxdepth=6
ss=length(labels)
trees=[]
for i=1:B
    inds=sample([1:ss;],ss,replace=true) # BOOTSTRAP samples created
    push!(trees,build_tree(labels[inds], features[inds,:],0,5))
end
function apply_trees(trees,features)
   dec=hcat([apply_tree(t,features) for t in trees ]...);
   #decm=reshape(dec,div(length(dec),B))
    vec(mapslices(majority_vote,dec,2)) ## Majority vote

end
@show confusion_matrix(labelsTest,apply_trees(trees,featuresTest)).accuracy


#USING RANDOM FOREST
rf_model = build_forest(labels, features, 2, 50, 1.0, 6)
# using 2 random features, 50 trees, 1.0 portion of samples per tree (optional), and a maximum tree depth of 6 (optional)
# apply learned model
@show confusion_matrix(labelsTest,apply_forest(rf_model,featuresTest)).accuracy


#@show head(boston)



#bagged =     # make sure to save the result of question 1 in `bagged`
#rf =         # make sure to save the result of question 2 in `rf`

#bagged =     # make sure to save the result of question 1 in `bagged`
#rf =         # make sure to save the result of question 2 in `rf`
