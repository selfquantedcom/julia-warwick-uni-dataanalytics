hhtenure[:ndep] = zeros(nrow(hhtenure))
hhtenure[:dep] = zeros(nrow(hhtenure))
hhtenure[:older] = zeros(nrow(hhtenure))
hhtenure[:own] = zeros(nrow(hhtenure))
hhtenure[:sr] = zeros(nrow(hhtenure))
hhtenure[:pr] = zeros(nrow(hhtenure))

length = nrow(hhtenure)
width = ncol(hhtenure)
#calculate ndep
#include the following for ndep
#sp	Households without dependents	Single person under 65
#cpnc		Couple with no children
#oth
#cpndc	Households with non-dependent children	Couple with non-dependent children
#lpndc


for i in 1:length
    for j in 1:width
        name = string(names(hhtenure)[j])

         if(startswith(name,"sp"))
            if(startswith(name,"sp65"))
                continue
            else
                hhtenure[i,33] += hhtenure[i,j]
            end
        end

        if(startswith(name,"cpnc"))
            hhtenure[i,33] += hhtenure[i,j]
        end

        if(startswith(name,"oth"))
            if(startswith(name,"othdc"))
                continue
            else
                hhtenure[i,33] += hhtenure[i,j]
            end
        end

        if(startswith(name,"cpndc"))
            hhtenure[i,33] += hhtenure[i,j]
        end

        if(startswith(name,"lpndc"))
            hhtenure[i,33] += hhtenure[i,j]
        end
    end
end

#calculate dep
#include the following for dep
#cpdc	Households with dependent children	Couple with dependent children
#lpdc		Lone parent with dependent children
#othdc

for i in 1:length
    for j in 1:width
        name = string(names(hhtenure)[j])

        if(startswith(name,"cpdc"))
            hhtenure[i,34] += hhtenure[i,j]
        end

        if(startswith(name,"lpdc"))
            hhtenure[i,34] += hhtenure[i,j]
        end

        if(startswith(name,"othdc"))
            hhtenure[i,34] += hhtenure[i,j]
        end
    end
end

#older
#include the following
#sp65	Older households	Single person aged 65 or over
#cp65

for i in 1:length
    for j in 1:width
        name = string(names(hhtenure)[j])

        if(startswith(name,"sp65"))
            hhtenure[i,35] += hhtenure[i,j]
        end

        if(startswith(name,"cp65"))
            hhtenure[i,35] += hhtenure[i,j]
        end
    end
end


#own


for i in 1:length
    for j in 1:35
        name = string(names(hhtenure)[j])

        if(endswith(name,"own"))
            hhtenure[i,36] += hhtenure[i,j]
        end
    end
end

#sr

for i in 1:length
    for j in 1:36
        name = string(names(hhtenure)[j])

        if(endswith(name,"sr"))
            hhtenure[i,37] += hhtenure[i,j]
        end
    end
end

#pr
for i in 1:length
    for j in 1:37
        name = string(names(hhtenure)[j])

        if(endswith(name,"pr"))
            hhtenure[i,38] += hhtenure[i,j]
        end
    end
end







# YOUR CODE HERE
hhtenure[:prop_dep] = zeros(nrow(hhtenure))
hhtenure[:prop_ndep] = zeros(nrow(hhtenure))
hhtenure[:prop_older] = zeros(nrow(hhtenure))
hhtenure[:prop_own] = zeros(nrow(hhtenure))
hhtenure[:prop_sr] = zeros(nrow(hhtenure))
hhtenure[:prop_pr] = zeros(nrow(hhtenure))

length = nrow(hhtenure)
#total column at j=32

for i in 1:length
    hhtenure[i,39] = hhtenure[i,34]/hhtenure[i,32]
    hhtenure[i,40] = hhtenure[i,33]/hhtenure[i,32]
    hhtenure[i,41] = hhtenure[i,35]/hhtenure[i,32]
    hhtenure[i,42] = hhtenure[i,36]/hhtenure[i,32]
    hhtenure[i,43] = hhtenure[i,37]/hhtenure[i,32]
    hhtenure[i,44] = hhtenure[i,38]/hhtenure[i,32]
end


# YOUR CODE HERE
hhtenure[:sl_oth] = zeros(nrow(hhtenure))
hhtenure[:sl_lpdc] = zeros(nrow(hhtenure))
hhtenure[:sl_sr] = zeros(nrow(hhtenure))
hhtenure[:sl_pr] = zeros(nrow(hhtenure))
hhtenure[:oth] = zeros(nrow(hhtenure)) #49
hhtenure[:lpdc] = zeros(nrow(hhtenure)) #50

#sr 37,pr 38, total 32
#sl_sr 47
length = nrow(hhtenure)
#48 col

#calculate oth
for i in 1:length
    for j in 1:width
        name = string(names(hhtenure)[j])
        if(startswith(name,"oth"))
            hhtenure[i,49] += hhtenure[i,j]
        end
    end
end

#calculate lpdc
for i in 1:length
    for j in 1:width
        name = string(names(hhtenure)[j])
        if(startswith(name,"lpdc"))
            hhtenure[i,50] += hhtenure[i,j]
        end
    end
end



for i in 1:length
    #sl_sr is sr/total:sr
    hhtenure[i,47] = 195*hhtenure[i,37]/sum(hhtenure[:sr])
    #sl_pr is pr/total:pr
    hhtenure[i,48] = 195*hhtenure[i,38]/sum(hhtenure[:pr])
    #sl_oth
    hhtenure[i,45] = 195*hhtenure[i,49]/sum(hhtenure[:oth])
    #sl_ldpc
    hhtenure[i,46] = 195*hhtenure[i,50]/sum(hhtenure[:lpdc])
end


# YOUR CODE HERE
n_oth = 0
n_lpdc = 0
n_sr = 0
n_pr = 0

for i in 1:length
    if(hhtenure[i,45]>1)
        n_oth += 1
    end

    if(hhtenure[i,46]>1)
        n_lpdc += 1
    end

    if(hhtenure[i,47]>1)
        n_sr += 1
    end

    if(hhtenure[i,48]>1)
        n_pr += 1
    end
end


#q2
# YOUR CODE HERE
ethnicity[:total] = zeros(nrow(ethnicity))
ethnicity[:sl_wother] = zeros(nrow(ethnicity)) #17
ethnicity[:sl_indian] = zeros(nrow(ethnicity)) # 18 ..
ethnicity[:sl_pakist] = zeros(nrow(ethnicity))
ethnicity[:sl_bangla] = zeros(nrow(ethnicity))
ethnicity[:sl_blafrican] = zeros(nrow(ethnicity))

#total column
#length = 195

#names(hhtenure)[])

for i in 1:length
    #wother 4
    ethnicity[i,17] = 195*ethnicity[i,4]/sum(ethnicity[:wother])

    ethnicity[i,18] = 195*ethnicity[i,6]/sum(ethnicity[:indian])

    ethnicity[i,19] = 195*ethnicity[i,7]/sum(ethnicity[:pakist])

    ethnicity[i,20] = 195*ethnicity[i,8]/sum(ethnicity[:bangla])

    ethnicity[i,21] = 195*ethnicity[i,11]/sum(ethnicity[:blafrican])
end

#head(ethnicity)


# YOUR CODE HERE
wother_min = ethnicity[1,17]
wother_max = ethnicity[1,17]

for i in 1:195
    if(ethnicity[i,17]<wother_min)
        wother_min = ethnicity[i,17]
    end

    if(ethnicity[i,17]>wother_max)
        wother_max = ethnicity[i,17]
    end
end


wother_mean = mean(ethnicity[:sl_wother])
wother_sd = std(ethnicity[:sl_wother])

indian_mean = mean(ethnicity[:sl_indian])
indian_sd = std(ethnicity[:sl_indian])
indian_min = ethnicity[1,18]
indian_max = ethnicity[1,18]

for i in 1:195
    if(ethnicity[i,18]<indian_min)
        indian_min = ethnicity[i,18]
    end

    if(ethnicity[i,18]>indian_max)
        indian_max = ethnicity[i,18]
    end
end

pakist_mean = mean(ethnicity[:sl_pakist])
pakist_sd = std(ethnicity[:sl_pakist])
pakist_min = ethnicity[1,19]
pakist_max = ethnicity[1,19]

for i in 1:195
    if(ethnicity[i,19]<pakist_min)
        pakist_min = ethnicity[i,19]
    end

    if(ethnicity[i,19]>pakist_max)
        pakist_max = ethnicity[i,19]
    end
end

bangla_mean = mean(ethnicity[:sl_bangla])
bangla_sd = std(ethnicity[:sl_bangla])
bangla_min = ethnicity[1,20]
bangla_max = ethnicity[1,20]

for i in 1:195
    if(ethnicity[i,20]<bangla_min)
        bangla_min = ethnicity[i,20]
    end

    if(ethnicity[i,20]>bangla_max)
        bangla_max = ethnicity[i,20]
    end
end

blafrican_mean = mean(ethnicity[:sl_blafrican])
blafrican_sd = std(ethnicity[:sl_blafrican])
blafrican_min = ethnicity[1,21]
blafrican_max = ethnicity[1,21]

for i in 1:195
    if(ethnicity[i,21]<blafrican_min)
        blafrican_min = ethnicity[i,21]
    end

    if(ethnicity[i,21]>blafrican_max)
        blafrican_max = ethnicity[i,21]
    end
end

# YOUR CODE HERE
h1 = histogram(ethnicity[:sl_wother] , label="sl_wother", yaxis="frequency")
h2 = histogram(ethnicity[:sl_indian] , label="sl_indian", color="red")
h3 = histogram(ethnicity[:sl_pakist] , label="sl_pakist", color="yellow")
h4 = histogram(ethnicity[:sl_bangla] , label="sl_bangla", xaxis="standard. likelihood", yaxis="frequency", color="green")
h5 = histogram(ethnicity[:sl_blafrican] , label="sl_blafrican", xaxis="standard. likelihood", color="purple" )

plot(h1, h2, h3, h4, h5)

s1 = scatter(ethnicity[:sl_wother] , ethnicity[:IMD], yaxis="IMD", label="sl_wother", smooth=true)
s2 = scatter(ethnicity[:sl_indian] , ethnicity[:IMD], label="sl_indian", color="red", smooth=true)
s3 = scatter(ethnicity[:sl_pakist] , ethnicity[:IMD], label="sl_pakist", color="yellow", smooth=true)
s4 = scatter(ethnicity[:sl_bangla] , ethnicity[:IMD], yaxis="IMD", xaxis="standard. likelyhood", label="sl_bangla", color="green", smooth=true)
s5 = scatter(ethnicity[:sl_blafrican] , ethnicity[:IMD], xaxis="standard. likelyhood", label="sl_blafrican", color="purple", smooth=true )

plot(s1, s2, s3, s4, s5)

#There seems to be a positive relationship between the concentration of an ethnic group
#and the Index of Multiple Deprivation. Higher levels of concentration are associated with
#higher IMD for all ethnic groups but the Indian ethnic group.

# YOUR CODE HERE
@show median(ethnicity[:sl_wother])
@show median(ethnicity[:sl_indian])
@show median(ethnicity[:sl_pakist])
@show median(ethnicity[:sl_bangla])
@show median(ethnicity[:sl_blafrican])

bar([(1.0, median(ethnicity[:sl_wother])), (2.0, median(ethnicity[:sl_indian])), (3.0, median(ethnicity[:sl_pakist])), (4.0, median(ethnicity[:sl_bangla])), (5.0, median(ethnicity[:sl_blafrican]))], label="ethnic groups", xaxis="1=sl_wother, 2=sl_indian, 3=sl_pakist, 4=sl_bangla, 5=sl_blafrican", yaxis="median")

Due to the skew of the distribution, median of the standardised likelyhood appears to be a good choice
of a summary statistics. Based on this meassure the most concentrated ethnic group
appears to be the Indian ethnic group; confirming our findings from previous example. 
